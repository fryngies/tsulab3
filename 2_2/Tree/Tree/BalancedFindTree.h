#pragma once

#include "Node.h"
#include "FindTree.h"

class BalancedFindTree : public FindTree
{
	Node* root;

	Node* addBalance(Node*, int, int&);

public:
	BalancedFindTree() : root(nullptr), FindTree() {}
	BalancedFindTree(int n) : root(nullptr), FindTree() {
		genrandom(n);
	}
	~BalancedFindTree() {
		deltree(root);
	}

	Node*& getroot() {
		return root;
	}

	Node* add(Node* p, int k) {
		int h = 1; // balance flag

		if (root == nullptr)
			root = new Node(k);

		return root = addBalance(p, k, h);
	}
};
