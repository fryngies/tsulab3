#pragma once

#include <vector>
#include "FormTree.h"
#include "BinaryTree.h"
#include "Node.h"

class HuffTree : public BinaryTree {
	std::vector<Node*> v;
	int* a; // ������ ������ ��������

	int m; // num of bytes in src file
	int n; // num of bits in dest file

	Node* root;

public:
	HuffTree() : a(nullptr), root(nullptr), m(0), n(0), BinaryTree() {
	}
	~HuffTree();

	Node*& getroot() {
		return root;
	}

	int* periodicity(char const*);
	std::string create_list();
	void create_tree();
	std::string code(char const*, char const*);
	std::string decode(char const*);
	float compression_ratio();
};
