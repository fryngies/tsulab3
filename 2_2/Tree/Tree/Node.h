﻿#pragma once

#include <bitset>

class Node {
	int key;
	int balance = 0;
	std::bitset<256> s;

	Node* left;
	Node* right;

public:
	Node(int x = 0, Node* l = nullptr, Node* r = nullptr) : key(x), left(l), right(r) {}
	Node(int k, char c, Node* l = nullptr, Node* r = nullptr) : key(k), left(l), right(r) {
		s.set(c);
	}

	friend class FormTree;
	friend class BinaryTree;
	friend class FindTree;
	friend class BalancedFindTree;
	friend class HuffTree;
	friend bool cmpr(Node*, Node*);
};
