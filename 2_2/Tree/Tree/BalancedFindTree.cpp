#include "Node.h"
#include "BalancedFindTree.h"

Node* BalancedFindTree::addBalance(Node* p, int k, int& h) {
	if (!p) {
		p = new Node(k);
		h = 1;
		return p;
	}

	Node *q, *s;

	if (k == p->key) { // found - return
		h = 0;
		return p;
	}

	if (k < p->key) { // if less go left
		p->left = addBalance(p->left, k, h); // add for left

		if (!h)
			return p; // no need in balancing

		if (p->balance == 1) {
			p->balance = 0; // update balance
			h = 0; // no need in balancing
			return p;
		}

		if (p->balance == 0) {
			p->balance = -1; // update balance

			return p;
		}

		p->balance = -2;
		s = p->left;
		if (s->balance == -1) {
			p->left = s->right; // rotate right
			s->right = p;
			p->balance = 0;
			s->balance = 0;
			p = s;
		} else {
			q = s->right; // double rotate left-right

			s->right = q->left;
			q->left = s;
			p->left = q->right;
			q->right = p;
			if (q->balance == -1) {
				p->balance = 1;
				s->balance = 0;
			}
			if (q->balance == 1) {
				p->balance = 0;
				s->balance = -1;
			}
			if (q->balance == 0) {
				p->balance = 0;
				s->balance = 0;
			}
			q->balance = 0;
			p = q;
		}

		h = 0;

		return p;
	} else {
		p->right = addBalance(p->right, k, h);
		if (!h)
			return p;
		if (p->balance == -1) {
			h = 0;
			p->balance = 0;
			return p;
		}
		if (p->balance == 0) {
			p->balance = 1;
			return p;
		}

		p->balance = 2;
		s = p->right;
		if (s->balance == 1) {
			p->right = s->left;
			s->left = p;
			p->balance = 0;
			s->balance = 0;
			p = s;
		} else {
			q = s->left;

			s->left = q->right;
			q->right = s;
			p->right = q->left;
			q->left = p;
			if (q->balance == 1) {
				p->balance = -1;
				s->balance = 0;
			}
			if (q->balance == -1) {
				p->balance = 0;
				s->balance = 1;
			}
			if (q->balance == 0) {
				p->balance = 0;
				s->balance = 0;
			}
			q->balance = 0;
			p = q;
		}

		h = 0;

		return p;
	}

	return p;
}
