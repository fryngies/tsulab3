#pragma once

#include "BinaryTree.h"

class FindTree : public BinaryTree {
	Node* root;

public:
	FindTree(int*, int); // ����������� ������ ������ �� �������
	FindTree() : root(nullptr) {}
	FindTree(int n) : root(nullptr) {
		genrandom(n);
	}
	~FindTree() {
		deltree(root);
	}

	Node*& getroot() {
		return root;
	}

	Node* add(Node*, int); // ���������� (� ������ ������������� �������)
	void genrandom(int, int lim=100);
	int find(Node*, int); // ����� ������, �� ������� ��������� ������� ����
	Node* del(Node*, int); // �������� ����� (������)
	void delsubtree(Node*, int); // ������� ��������� ����, � ������� ����

	int min(Node*); // ��������������
	int max(Node*); // ��������������
	void obhod(Node*); // �����

	FindTree operator+=(int);

	Node* replace_with_array(Node*, int*, int&);
	/*	�������� �������, ������� ��������� ����� �� �������,
		�������������� �� �����������, � �������� ���������
		������ ������
		(�������� ���� ���, ����� ������ �������� ������� ������)
		(���������� ����� � ������� ����� ����� ����� � ������)	*/
};
