#include <iostream>
#include <cstdlib>
#include <vector>
#include <ctime>
#include "BinaryTree.h"

BinaryTree::~BinaryTree() {
	deltree(getroot());
}

Node* BinaryTree::add(Node* p, int k) {
	if (p == nullptr)
		return new Node(k);

	switch (rand() % 2) {
	case 0:
		p->left = add(p->left, k);
		break;
	case 1:
		p->right = add(p->right, k);
	}

	return p;
}

Node*& BinaryTree::getroot() {
	return root;
}

void BinaryTree::genrandom(int n, int lim) {
	srand((unsigned int)time(NULL));
	int num = rand() % lim;

	// check for root first
	if (n-- && root == nullptr)
		root = add(root, num);
	// add as many as need
	while (n--) {
		num = rand() % lim;
		while (find(root, num) != -1)
			num = rand() % lim;
		add(root, num);
	}
}

void BinaryTree::deltree(Node* p) {
	if (p == nullptr)
		return;

	deltree(p->left);
	deltree(p->right);

	delete p;
}

int BinaryTree::nnode() {
	return nnode(root);
}

int BinaryTree::nnode(Node* p) {
	if (p == nullptr)
		return 0;

	return nnode(p->left) + nnode(p->right) + 1;
}

int BinaryTree::height(Node* p) {
	if (p == nullptr)
		return 0;

	int l = height(p->left);
	int r = height(p->right);

	return (l > r ? l : r) + 1;
}

bool BinaryTree::balance(Node* p) {
	if (p == nullptr)
		return true;

	int l = height(p->left);
	int r = height(p->right);

	if (abs(r - l) > 1)
		return false;
	if (balance(p->left) == false)
		return false;

	return balance(p->right);
}

bool BinaryTree::ideal(Node* p) {
	if (p == nullptr)
		return true;

	int l = nnode(p->left);
	int r = nnode(p->right);
	if (abs(r - l) > 1)
		return false;
	if (ideal(p->left) == 0)
		return false;

	return ideal(p->right);
}

int BinaryTree::find(Node* p, int k) {
	return findBinary(p, 0, k);
}

int BinaryTree::findBinary(Node* p, int s, int k) {
	if (p == nullptr)
		return -1;

	if (p->key == k)
		return s;

	int l = findBinary(p->left, s + 1, k);
	if (l != -1)
		return l;
	return findBinary(p->right, s + 1, k);
}

int BinaryTree::min(Node* p) {
	int a;

	min(p, a);

	return a;
}

void BinaryTree::min(Node* p, int& a) {
	if (p == nullptr)
		return;

	min(p->left, a);
	min(p->right, a);
	a = a < p->key ? a : p->key;
}

int BinaryTree::max(Node* p) {
	if (p == nullptr)
		return NULL;

	int m1 = max(p->left);
	int m2 = max(p->right);
	m1 = m1 > m2 ? m1 : m2;
	m1 = m1 > p->key ? m1 : p->key;

	return m1;
}

int BinaryTree::sum(Node* p) {
	if (p == nullptr)
		return NULL;

	int l = sum(p->left);
	int r = sum(p->right);

	return l + r + p->key;
}

void BinaryTree::even_in_array(Node* p, int* a, int& k) {
	if (p == nullptr)
		return;

	even_in_array(p->left, a, k);
	if (!(p->key % 2))
		a[k++] = p->key;
	even_in_array(p->right, a, k);
}

void BinaryTree::bypassByLevel() {
	int h = height(root);
	for (int i = 0; i < h; ++i) {
		printl(root, i, 0);
		std::cout << std::endl;
	}
}

std::vector<int>* BinaryTree::list(Node* p, std::vector<int>* l) {
	if (p == nullptr)
		return l;
	if (p->left == nullptr && p->right == nullptr) {
		l->push_back(p->key);
		return l;
	}
	list(p->left, l);
	list(p->right, l);

	return l;
}

void BinaryTree::printl(Node* p, int h, int i) {
	if (p == nullptr)
		return;
	if (i == h) {
		std::cout << p->key << ' ';
		return;
	}
	printl(p->left, h, i + 1);
	printl(p->right, h, i + 1);
}

void BinaryTree::printv(Node* p, int l) {
	if (p == nullptr)
		return;

	printv(p->right, l + 3); // ����� ������� ���������, ������ +3

	for (int k = 0; k < l; ++k)
		std::cout << " ";
	std::cout.width(2);
	std::cout << p->key << std::endl << std::endl;

	printv(p->left, l + 3);
}

BinaryTree BinaryTree::operator+=(int k) {
	root == nullptr ? root = add(root, k) : add(root, k);

	return *this;
}
