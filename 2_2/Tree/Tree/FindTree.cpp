#include <iostream>
#include <cstdlib>
#include <ctime>
#include "FindTree.h"

FindTree::FindTree(int* a, int n) {
	root = nullptr;
	for (int i = 0; i < n; ++i)
		*this += a[i];
}

Node* FindTree::add(Node* p, int k) {
	Node* y = nullptr;

	// look up first
	while (p != nullptr) {
		if (k == p->key) {
			return p;
		} else {
			y = p;
			if (k < p->key)
				p = p->left;
			else
				p = p->right;
		}
	}

	// add new if not found
	Node* nn = new Node(k);
	if (!y)
		root = nn;
	else {
		if (k < y->key)
			y->left = nn;
		else
			y->right = nn;
	}

	return p;
}

void FindTree::genrandom(int n, int lim) {
	srand((unsigned int)time(NULL));

	while (n--) {
		int num = rand() % lim;
		while (find(getroot(), num) != -1)
			num = rand() % lim;
		add(getroot(), num);
	}
}

int FindTree::find(Node* p, int k) {
	int s = 1;

	while (p) {
		if (p->key == k)
			return s;
		++s;

		if (k < p->key)
			p = p->left;
		else
			p = p->right;
	}

	return -1;
}

void FindTree::obhod(Node* p) {
	if (!p)
		return;

	obhod(p->left);
	std::cout << p->key << " ";
	obhod(p->right);
}

Node* FindTree::del(Node *p, int k) // ������� ���������� ������� � ������, ��������, ���������, ������ � �� ����� ���������, ��������� ��, ��� �������� � ���������
{
	if (!p)
		return p;

	if (k == p->key) {
		Node* q;

		if (!p->left) //���� ��� ������ �������
		{
			q = p->right;
			delete p;
			return q;
		}

		if (!p->right) //���� ��� ������� �������
		{
			q = p->left;
			delete p;
			return q;
		}

		Node* s = p->right; //���� ��� ����, ���������� ������

		if (!s->left) //���� ��� ������ �������
		{
			p->key = s->key;
			p->right = s->right;
			delete s;
			return p;
		}

		q = s->left; //����� ������ (������)
		while (q->left)
		{
			s = q;
			q = q->left;
		}

		p->key = q->key;
		s->left = q->right;
		delete q;

		return p;
	}

	if (k < p->key)
		p->left = del(p->left, k);
	else
		p->right = del(p->right, k);

	return p;
}

int FindTree::min(Node* p) {
	if (!p)
		return -1;

	while (p->left)
		p = p->left;

	return p->key;
}

int FindTree::max(Node* p) {
	if (!p)
		return -1;

	while (p->right)
		p = p->right;

	return p->key;
}

void FindTree::delsubtree(Node* p, int k) {
	if (!p)
		return;

	if (p->key == k) {
		deltree(p);
		return;
	}

	if (p->key > k)
		delsubtree(p->left, k);
	else
		delsubtree(p->right, k);
}

Node* FindTree::replace_with_array(Node* p, int* a, int& i) {
	if (p == nullptr)
		return p;

	p->left = replace_with_array(p->left, a, i);
	p->key = a[i++];
	p->right = replace_with_array(p->right, a, i);

	return p;
}

FindTree FindTree::operator+=(int k) {
	add(root, k);

	return *this;
}
