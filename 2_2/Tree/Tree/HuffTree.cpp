#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include "HuffTree.h"

bool cmpr(Node *u1, Node *u2) {
	return u1->key < u2->key;
}

HuffTree::~HuffTree() {
	for (auto el : v)
		delete el;
	delete root;
}

int* HuffTree::periodicity(char const* namef) {
	const int alen = 256;
	if (a)
		delete[] a;
	a = new int[alen];
	for (int i = 0; i < alen; ++i)
		a[i] = 0;

	std::ifstream f(namef);
	if (!f)
		throw std::runtime_error("error opening file");

	char r;
	while (f.get(r)) {
		a[(unsigned int)r]++;
		++m;
	}

	f.close();

	return a;
}

std::string HuffTree::create_list() {
	for (int i = 0; i < 256; ++i)
		if (a[i]) {
			Node *p = new Node(a[i], char(i));
			v.push_back(p);
		}

	std::sort(v.begin(), v.end(), cmpr);

	std::string res = "";
	for (std::vector<Node*>::iterator pv = v.begin(); pv != v.end(); ++pv) {
		Node* q = *pv;

		for (int i = 0; i < 256; i++)
			if ((q->s)[i])
				res += std::to_string(i) + ' ';
	}
	res.pop_back(); // remove last char

	return res;
}

void HuffTree::create_tree() {
	Node* p;
	int d = 2;
	std::vector<Node*>::iterator pv = v.begin();

	int k = v.size();

	while (k-- > 1) {
		Node* p1 = *pv;
		Node* p2 = *(pv + 1);

		p = new Node(p1->key + p2->key, 0, p1, p2);

		p->s = p1->s | p2->s;
		v.push_back(p);

		pv = v.begin();
		pv += d;
		d += 2;

		std::sort(pv, v.end(), cmpr);
	}

	root = p;
}

std::string HuffTree::code(char const* namef1, char const* namef2) {
	std::ifstream f(namef1);
	std::ofstream ff(namef2);

	Node* p = root; // node pointer
	int L = 8; // code residual length
	n = 0; // num of bits
	char r; // symbol
	unsigned char tmp = 0; // code
	std::string res = ""; // result in std::string

	while (f.get(r)) { // get symbol
		p = root; // start over

		while (p->left) { // search for symbol
			if ((p->left->s)[(unsigned char)r]) { // if found left go left
				res += '0'; // add 0 to code
				tmp = tmp << 1;

				p = p->left;

				--L;
				++n;
			} else {
				res += '1'; // add 1 to code
				tmp = (tmp << 1) | 1;

				p = p->right;

				--L;
				++n;
			}

			// byte formed
			if (!L) {
				ff << tmp; // dropping
				L = 8;
			}
		}
	}

	// if byte not done
	if (L) {
		// just fill it with zeros
		while (L) {
			tmp = tmp << 1;
			L--;
		}
		// and drop
		ff << tmp;
	}

	return res;
}

std::string HuffTree::decode(char const* namef1) {
	std::ifstream f(namef1);

	Node* p = root;

	int k = n; // number of bits
	int L = 7; // code length-1
	int flg = 0; // symbol done
	char r; // byte

	std::string res = "";

	f.get(r); // get byte
	while (f) {
		char tmp = 1;

		if (flg == 1) { // get new byte from file if current is over
			f.get(r);
			flg = 0;
		}

		while (p->left && L >= 0) { // decode byte
			if (r & (tmp << L))
				p = p->right; // if 1 go right
			else 
				p = p->left; // else left

			--L; // --code length
			--k; // --file size

			// eof
			if (k < 0)
				return res;
		}

		// byte done
		if (L == -1) {
			flg = 1;
			L = 7;
		}

		// symbol found
		if (!(p->left)) {
			char tm;
			for (tm = 0; tm < 256 && !(p->s)[tm]; ++tm); // find symbol num

			res += (char)tm; // add to result

			p = root; // start over
		}
	}

	return res;
}

float HuffTree::compression_ratio() {
	if (!n || !m)
		return 0;

	// new len / old len in bytes * 100
	return float(n) / (m * 8) * 100;
}
