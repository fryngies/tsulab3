#pragma once

#include <cstdlib>
#include <string>
#include <vcclr.h>
#include "Node.h"

struct LinkedNode {
	Node* p;
	LinkedNode* next;
};

class FormTree {
	typedef System::Drawing::Color Color;

	// NodeODO: Read what gcroot is.
	// type-safe wrapper template
	gcroot<Color> color = System::Drawing::Color::Blue;
	int nodeDiam = 30;

	void DrawNode(System::Drawing::Graphics^, System::Drawing::Pen^, System::String^, int, int);
	void PrintLevel(System::Drawing::Graphics^, System::Drawing::Pen^, Node*, int, int, int&, int);
	// by level 2
	int Formheight(Node*);

public:
	FormTree(Color c=System::Drawing::Color::Blue, int diam=30) : color(c), nodeDiam(diam) {}

	void PrintT(System::Drawing::Graphics^, Node*, int, int, int, int);
	void PrintByLevel(System::Drawing::Graphics^, Node*, int x=10, int y=10);
	std::string PrintByLevel2(Node*);
};
