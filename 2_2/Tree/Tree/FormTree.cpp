#include "FormTree.h"

using namespace System::Drawing;

void FormTree::DrawNode(Graphics^ gr, Pen^ pen, System::String^ str, int x, int y) {
	gr->DrawEllipse(pen, x, y, nodeDiam, nodeDiam);
	Brush^ br_e = gcnew SolidBrush(color);
	gr->FillEllipse(br_e, x, y, nodeDiam, nodeDiam);

	Font^ f = gcnew Font("Arial", 12);
	// NodeODO: Contrast
	Brush^ br = gcnew SolidBrush(Color::White);
	// NodeODO: Count width
	gr->DrawString(str, f, br, (float)(x + nodeDiam / 2 - f->Size), (float)(y + nodeDiam / 2 - f->Size));
}

// TODO: Replace all magic numbers
void FormTree::PrintT(Graphics^ gr, Node* u, int l, int r, int y, int x_r) {
	if (u == nullptr)
		return;

	int x = (l + r) / 2;

	int s = 4;
	int x1 = x - s;
	int y1 = y - s;
	int s_y = 50;

	//Pen^ p = gcnew Pen(color, 2);
	//gr->DrawEllipse(p, x1, y1, d, d);
	//Brush^ br_e = gcnew SolidBrush(color);
	//gr->FillEllipse(br_e, x1, y1, d, d);
	Pen^ pen = gcnew Pen(color, 2);
	DrawNode(gr, pen, System::Convert::ToString(u->key), x1, y1);

	if (x_r > 0)
		gr->DrawLine(pen, x_r, y1 - s_y + nodeDiam, x1 + nodeDiam / 2, y1);

	PrintT(gr, u->left, l, x, y + 50, x1 + nodeDiam / 2);
	PrintT(gr, u->right, x, r, y + 50, x1 + nodeDiam / 2);
}

int FormTree::Formheight(Node* p) {
	if (p == nullptr)
		return 0;

	int l = Formheight(p->left);
	int r = Formheight(p->right);

	return (l > r ? l : r) + 1;
}


void FormTree::PrintLevel(Graphics^ gr, Pen^ pen, Node* node, int h, int i, int& offset_x, int offset_y) {
	if (node == nullptr)
		return;

	if (i == h) {
		DrawNode(gr, pen, System::Convert::ToString(node->key), offset_x, offset_y);
		offset_x += 50;
		return;
	}

	PrintLevel(gr, pen, node->left, h, i + 1, offset_x, offset_y);
	PrintLevel(gr, pen, node->right, h, i + 1, offset_x, offset_y);
}


void FormTree::PrintByLevel(Graphics^ gr, Node* node, int x, int y) {
	if (node == nullptr)
		return;

	Pen^ pen = gcnew Pen(color, 2);
	//gr->DrawEllipse(pen, x, x, d, d);
	//Brush^ br_e = gcnew SolidBrush(color);
	//gr->FillEllipse(br_e, x, y, d, d);
	//DrawNode(gr, pen, System::Convert::ToString(node->key), x, y);

	int h = Formheight(node);

	for (int i = 0, offset_y = 0, offset_x = 0; i < h; ++i, offset_x = 0, offset_y += (int)(nodeDiam * 1.5))
		PrintLevel(gr, pen, node, i, 0, offset_x, offset_y);
}

std::string FormTree::PrintByLevel2(Node* node) {
	LinkedNode *t = new LinkedNode;
	std::string res = "";
	t->p = node;
	LinkedNode* h = t;
	h->next = nullptr;

	Pen^ pen = gcnew Pen(color, 2);

	while (h) {
		if (h->p->left) {
			t->next = new LinkedNode;
			t = t->next;
			t->p = h->p->left;
		}
		if (h->p->right) {
			t->next = new LinkedNode;
			t = t->next;
			t->p = h->p->right;
		}
		res += std::to_string(h->p->key) + " ";
		t->next = nullptr;
		h = h->next;
	}

	return res;
}
