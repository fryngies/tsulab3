﻿#pragma once

#include <cstdlib>
#include <vector>
#include "FormTree.h"
#include "Node.h"

class BinaryTree {
	Node* root;

	int findBinary(Node*, int, int);

	void min(Node*, int&);

public:
	BinaryTree() : root(nullptr) {}
	BinaryTree(int n) : root(nullptr) {
		genrandom(n);
	}
	~BinaryTree();

	virtual Node*& getroot();

	virtual Node* add(Node*, int);
	void deltree(Node*); // √-

	int nnode(); // количество вершин
	int nnode(Node*); // количество вершин в дереве с корнем заданным в параметре
	int height(Node*); // высота
	int sum(Node*); // сумма ключей вершины дерева
	int find(Node*, int); // если функция найдет ключ, то возвращает уровень
	virtual int min(Node*); // заносит min во второй аргумент √-
	virtual int max(Node*); // возвращает max √-

	void genrandom(int, int lim=100); // generate random tree with n vertexes and lim
	void even_in_array(Node*, int*, int&); // заности четное количество ключей в массив

	void printv(Node*, int); // вывод дерева
	void printl(Node*, int, int); // вывести ключи по уровню

	bool balance(Node*); // сбалансированное дерево — дерево, у которого высота левого и правого поддерева отличаются не более чем на 1
	bool ideal(Node*); // идеальное дерево — разница между левым и правыми узлами не более 1
	std::vector<int>* list(Node*, std::vector<int>*); // вывод ключей из листьев
	void bypassByLevel(); // обход и вывод ключей по уровню

	BinaryTree operator+=(int); // добавляет ключ к дереву
};
