#pragma once

namespace Tree {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for AskDialog
	/// </summary>
	public ref class AskDialog : public System::Windows::Forms::Form
	{
	public:
		String^ value = "";
		bool digitsOnly;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	public:
		Form^ oldform;
		AskDialog(String^ message, Form^ form, bool don)
		{
			InitializeComponent();
			messageLabel->Text = message;
			oldform = form;
			digitsOnly = don;
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AskDialog()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  messageLabel;
	private: System::Windows::Forms::TextBox^  valueTextBox;
	protected:

	private: System::Windows::Forms::Button^  submitButton;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->messageLabel = (gcnew System::Windows::Forms::Label());
			this->valueTextBox = (gcnew System::Windows::Forms::TextBox());
			this->submitButton = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// messageLabel
			// 
			this->messageLabel->AutoSize = true;
			this->messageLabel->Dock = System::Windows::Forms::DockStyle::Left;
			this->messageLabel->Location = System::Drawing::Point(3, 0);
			this->messageLabel->Name = L"messageLabel";
			this->messageLabel->Size = System::Drawing::Size(61, 20);
			this->messageLabel->TabIndex = 0;
			this->messageLabel->Text = L"Enter value";
			// 
			// valueTextBox
			// 
			this->valueTextBox->Dock = System::Windows::Forms::DockStyle::Top;
			this->valueTextBox->Location = System::Drawing::Point(3, 23);
			this->valueTextBox->Name = L"valueTextBox";
			this->valueTextBox->Size = System::Drawing::Size(178, 20);
			this->valueTextBox->TabIndex = 1;
			this->valueTextBox->TextChanged += gcnew System::EventHandler(this, &AskDialog::valueTextBox_TextChanged);
			this->valueTextBox->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &AskDialog::valueTextBox_KeyPress);
			// 
			// submitButton
			// 
			this->submitButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->submitButton->Enabled = false;
			this->submitButton->Location = System::Drawing::Point(187, 23);
			this->submitButton->Name = L"submitButton";
			this->submitButton->Size = System::Drawing::Size(94, 20);
			this->submitButton->TabIndex = 2;
			this->submitButton->Text = L"Submit";
			this->submitButton->UseVisualStyleBackColor = true;
			this->submitButton->Click += gcnew System::EventHandler(this, &AskDialog::submitButton_Click);
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 2;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->messageLabel, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->valueTextBox, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->submitButton, 1, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(284, 46);
			this->tableLayoutPanel1->TabIndex = 3;
			// 
			// AskDialog
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 46);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"AskDialog";
			this->Text = L"Submit";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &AskDialog::AskDialog_FormClosing);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void submitButton_Click(System::Object^  sender, System::EventArgs^  e) {
		if (valueTextBox->Text == "")
			return;

		Close();
	}
	//private: System::Void askDialog_Close(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {
	//	Hide();
	//	value = valueTextBox->Text;
	//	oldform->Show();
	//}
	private: System::Void AskDialog_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		Hide();
		value = valueTextBox->Text;
		//oldform->Show();
	}
	private: System::Void valueTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		// trigger submit button
		valueTextBox->Text == "" ? submitButton->Enabled = false : submitButton->Enabled = true;
	}
private: System::Void valueTextBox_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (!digitsOnly)
			return;

		if (e->KeyChar == '.') {
			if (this->valueTextBox->Text->Contains(".") && !this->valueTextBox->SelectedText->Contains("."))
				e->Handled = true;
		}
		// allow negative numbers
		else if (e->KeyChar == '-' && !(this->valueTextBox->Text->Contains("-"))) {
			e->Handled = true;
			valueTextBox->Text = "-" + valueTextBox->Text;
		}
		// accept only digits ".", "-" and the Backspace character
		else if (!Char::IsDigit(e->KeyChar) && e->KeyChar != 0x08) {
			e->Handled = true;
		}
}
};
}
