#include <cstdlib>
#include <string>
#include <fstream>
#include "cfstream.hpp"
#include "naturalsort.hpp"

void genrandomfile(char const* name, int len, int maxrange) {
	FILE *f = fopen(name, "w");
	if(!f)
		throw "ERR_FILE_OPEN";

	fprintf(f, "%d", rand() % maxrange);
	for(; len > 2; --len)
		fprintf(f, " %d", rand() % maxrange);

	fclose(f);
}

int main() {
	srand((uint)time(NULL));
	genrandomfile("test.txt", 40, 10000);

	std::ifstream fsi("test.txt");

	std::cout << fsi.rdbuf() << std::endl;
	fsi.clear();
	fsi.seekg(0, std::ios::beg);

	Natural_Merging_Sort(fsi);

	fsi.close();

	return 0;
}
