#include "naturalsort.hpp"

#include "cfstream.hpp"
#include <iostream>
#include <fstream>
#include <vector>

// read given two files and distribute by given two temporary files
void ffmerge(std::istream& fsi1, std::istream& fsi2, std::ostream& ftmp1, std::ostream& ftmp2) {
	std::ostream* currtmp = &ftmp1;
	int buff1 = 0;
	int buff2 = 0;
	
	if(!fsi1.eof())
		fsi1 >> buff1;
	if(!fsi2.eof())
		fsi2 >> buff2;
	
	bool rangeover1 = false;
	bool rangeover2 = false;

	while(!fsi1.eof() && !fsi2.eof()) {
		rangeover1 = rangeover2 = false;
		while(!rangeover1 && !rangeover2) {
			if (buff1 <= buff2) {
				*currtmp << buff1 << " " << std::flush;
				rangeover1 = isendrange(fsi1);
				fsi1 >> buff1;
			} else {
				*currtmp << buff2 << " " << std::flush;
				rangeover2 = isendrange(fsi2);
				fsi2 >> buff2;
			}
		}

		while(!rangeover1) {
			*currtmp << buff1 << " " << std::flush;
			rangeover1 = isendrange(fsi1);
    		fsi1 >> buff1;
		}
		while(!rangeover2) {
			*currtmp << buff2 << " " << std::flush;
			rangeover2 = isendrange(fsi2);
    		fsi2 >> buff2;
		}
		*currtmp << "` " << std::flush;

		currtmp = &(currtmp == &ftmp1 ? ftmp2 : ftmp1);
	}
	
	rangeover1 = rangeover2 = false;
	while(!rangeover1 && !fsi1.eof()) {
		*currtmp << buff1 << " " << std::flush;
		rangeover1 = isendrange(fsi1);
		fsi1 >> buff1;
	}
	while(!rangeover2 && !fsi2.eof()) {
		*currtmp << buff2 << " " << std::flush;
		rangeover2 = isendrange(fsi2);
		fsi2 >> buff2;
	}
	if (rangeover1 || rangeover2)
		*currtmp << "` " << std::flush;
}

bool isendrange(std::istream& f) {
	std::streampos oldpos = f.tellg();
	f.ignore();
	char tmp = f.get();

	if (tmp != '`') {
		f.seekg(oldpos);
		return false;
	}
	else {
		f.ignore();
		return true;
	}
}

void Natural_Merging_Sort(std::istream& fsi) {
	if (fsi.eof())
		throw std::runtime_error("File eof");

	std::vector<cfstream*> tmpfs {
		new cfstream("natsort1.tmp", std::fstream::out),
		new cfstream("natsort2.tmp", std::fstream::out),
		new cfstream("natsort3.tmp", std::fstream::out),
		new cfstream("natsort4.tmp", std::fstream::out),
	};
	int a1;
	int a2;

	fsi >> a1;
	*(tmpfs[0]) << a1 << " " << std::flush;

//	fsi.ignore();
	fsi >> a2;

	bool f2last = 1;
	while (!fsi.eof()) {
		if (a2 < a1) {
			if (f2last) {
				*(tmpfs[0]) << "` " << std::flush;
				f2last = false;
			} else {
				*(tmpfs[1]) << "` " << std::flush;
				f2last = true;
			}
		}

		if (f2last) {
			*(tmpfs[0]) << a2 << " " << std::flush;
		} else {
			*(tmpfs[1]) << a2 << " " << std::flush;
		}

		a1 = a2;
		fsi >> a2;
	}

	if (!f2last)
		*(tmpfs[1]) << "` " << std::flush;
	if (f2last)
		*(tmpfs[0]) << "` " << std::flush;

	if (f2last) {
		*(tmpfs[1]) << a2 << " " << std::flush;
		*(tmpfs[1]) << "` " << std::flush;
	} else {
		*(tmpfs[0]) << a2 << " " << std::flush;
		*(tmpfs[0]) << "` " << std::flush;
	}

	for(uint8_t i = 0; i < 2; ++i)
		tmpfs[i]->reset();
	for(auto fs : tmpfs)
		fs->appendmode(std::fstream::in);

	f2last = true;
	do {
		if (f2last) {
			tmpfs[2]->clearf();
			tmpfs[3]->clearf();
			ffmerge(*tmpfs[0], *tmpfs[1], *tmpfs[2], *tmpfs[3]);
			f2last = false;
		}
		else {
			tmpfs[0]->clearf();
			tmpfs[1]->clearf();
			ffmerge(*tmpfs[2], *tmpfs[3], *tmpfs[0], *tmpfs[1]);
			f2last = true;
		}
		tmpfs[0]->reset();
		tmpfs[1]->reset();
		tmpfs[2]->reset();
		tmpfs[3]->reset();
	} while(!tmpfs[1]->isempty() && !tmpfs[3]->isempty());

	if(!tmpfs[0]->isempty())
		tmpfs[0]->copy("result.txt");
	if(!tmpfs[2]->isempty())
		tmpfs[2]->copy("result.txt");

	for(auto fs : tmpfs)
		fs->remove();
}
