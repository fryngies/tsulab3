#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void showbits(uint8_t x) {
	for(int i = (sizeof(int8_t) * 8) - 1; i >= 0; --i)
		(x & (1u << i)) ? putchar('1') : putchar('0');
}

uint8_t c_asmtask(uint8_t b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;	// swap tetrades
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;	// swap all adjacent pairs
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;	// swap all adjacent single bits

   return b;
}

uint8_t asmtask(uint8_t);

int main() {
	uint8_t num = 200;

	printf("num:\t\t");
	showbits(num);
	printf("\n");

	uint8_t c_res = c_asmtask(num);
	printf("c_asmtask:\t");
	showbits(c_res);
	printf("\n");

	uint8_t res = asmtask(num);
	printf("asmtask:\t");
	showbits(res);
	printf("\n");

	printf("test ");
	printf(c_res != res ? "NOT " : "");
	printf("passed\n");
}

