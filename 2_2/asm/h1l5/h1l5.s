; Отразить тетрады
global _asmtask

section .text

_asmtask:
	default	rel

	; reverse:11
	mov		rcx, rdi
	and		rcx, 0xF0
	shr		rcx, 4		; extract 2nd tetrade, F0_16 = 1111 0000_2

	and		rdi, 0x0F
	shl		rdi, 4		; extract 1st tetrade, 0F_16 = 0000 1111_2

	or		rdi, rcx	; swap!

	; reverse:12
	mov		rcx, rdi
	and		rcx, 0xCC
	shr		rcx, 2		; extract pairs CC_16 = 1100 1100_2

	and		rdi, 0x33
	shl		rdi, 2		; extract pairs, 33_16 = 0011 0011_2

	or		rdi, rcx	; swap!

	; reverse:13
	mov		rcx, rdi
	and		rcx, 0xAA
	shr		rcx, 1

	and		rdi, 0x55
	shl		rdi, 1

	or		rdi, rcx

	; return result
	mov		rax, rdi
	ret

