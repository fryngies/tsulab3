; Проверить, есть ли цифра в числе
global _main

section .data
	itstrue:	db		`true`, 10
	.len:		equ		$ - itstrue
	itsfalse:	db		`false`, 10
	.len:		equ		$ - itsfalse

section .text
_main:
	mov		rax, 231
	jmp		docompare

docompare:
	xor		rdx, rdx
	mov		rsi, 10
	div		rsi

	cmp		rdx, 5
	je		eqtrue

	cmp		rax, 0
	je		eqfalse

	jmp		docompare

eqtrue:
	mov		rsi, itstrue
	mov		rdx, itstrue.len
	jmp		out

eqfalse:
	mov		rsi, itsfalse
	mov		rdx, itsfalse.len
	jmp		out

out:
	mov		rax, 0x2000004 ; write
	mov		rdi, 1 ; stdout
	syscall

	mov		rax, 0x2000001 ; exit
	mov		rdi, 0
	syscall

