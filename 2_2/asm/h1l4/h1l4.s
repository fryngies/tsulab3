; Даны два строго упорядоченных массива
; по возрастанию массива X и Y из n
; элементов каждый. В массиве X
; найти все такие X[k], для которых
; найдутся некоторые элементы:
; Y[i] < X[k] < Y[i+1]. Вычислить
; количество m таких элементов из X
; и переписать их в первые элементы C
global _asm1

section .text
_asm1:
	default	rel

	sub		rcx, 2
	mov		r9, -1			; setup counter

	mov		rax, 0			; setup result

	jmp		outerloop

; rdi, rsi, rdx, rcx, r8, r9
; rdi — arr1
; rsi — arr2
; rdx — arr3 — result array
; rcx — arrlen

; r9 — outer iterator
; r8 — inner iterator
; rcx — arrlen - 2

; rax — result length

outerloop:
	inc		r9			; inc outer iterator

	cmp		r9, rcx
	jg		out

	mov		r8, -1
	jmp		innerloop

innerloop:
	inc		r8							; inc counter

	cmp		r8, rcx
	jg		outerloop					; loop done

	mov		r10d, [rsi + 4 * r8 + 4]	; arr2[j+1]
	mov		r11d, [rdi + 4 * r9]		; arr1[i]

	cmp		r10d, r11d
	jle		innerloop					; if not arr2[j+1] > arr1[i+1]

	mov		r10d, [rsi + 4 * r8]		; arr2[j]

	cmp		r10d, r11d
	jge		innerloop					; if not arr2[j] < arr1[i]

	mov		[rdx + rax * 4], r11d		
	inc		rax							; arr3[result++] = arr1[i]

	jmp		innerloop					; loop

out:
	ret

