#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int asm1(int*, int*, int*, int);

// task solution in C
int ctask(int* arr1, int* arr2, int* result_arr, int arrlen) {
	int result_arr_len = 0;
	--arrlen;

	for (int i = 0; i < arrlen; ++i)
		for (int j = 0; j < arrlen; ++j)
			if (arr2[j] < arr1[i] && arr2[j+1] > arr1[i])
				result_arr[result_arr_len++] = arr1[i];

	return result_arr_len;
}

void printarr(int* arr, int arrlen) {
	for(int i = 0; i < arrlen; ++i)
		printf("%d ", arr[i]);
}

// generates random array in ascending order
void randarr(int* arr1, int arrlen, int lim) {
	arr1[0] = rand() % lim;
	for(int i = 1; i < arrlen; ++i)
		arr1[i] = arr1[i-1] + ( rand() % lim ) / 10 + 1;
}

int main() {
	const int arrlen = 8;		// number of elements
	const int lim = 100;

	int* arr1 = malloc(arrlen * sizeof(int));
	int* arr2 = malloc(arrlen * sizeof(int));
	int* result_arr = malloc(arrlen * sizeof(int));

	srand(time(NULL));

	for (int test_counter = 1; test_counter <= 100; ++test_counter) {
		randarr(arr1, arrlen, lim);
		randarr(arr2, arrlen, lim);

		int result_arr_test_len = ctask(arr1, arr2, result_arr, arrlen);
		int result_arr_len = asm1(arr1, arr2, result_arr, arrlen);

		/* if(!result_arr_len) */
		/* 	continue; */

		printf("%d:\n", test_counter);
		printf("arr1:\t");
		printarr(arr1, arrlen);
		printf("\n");
		printf("arr2:\t");
		printarr(arr2, arrlen);
		printf("\n");

		if (result_arr_len) {
			printf("Result:\t");
			for (int i = 0; i < result_arr_len; ++i)
				printf("%d ", result_arr[i]);
			printf("\n");
			printf("%d elements.\n", result_arr_len);
		} else {
			printf("No elements satisfying the given condition were found.\n");
		}
		printf("\n");

		if (result_arr_len != result_arr_test_len) {
			printf("Test failed.\n");
			printarr(result_arr, result_arr_test_len);
			printf("\n");
			return 1;
		}
	}
	printf("All tests are passed.\n");
}

