; Проверить, существует ли такое k,
; что 2^k = n.
extern _printf

global _main

section .data
	num:		dq		255
	itstrue:	db		`true`, 10
	.len:		equ		$ - itstrue
	itsfalse:	db		`false`, 10
	.len:		equ		$ - itsfalse

section .text
_main:
	default	rel
	jmp		comp

comp:
	mov		rax, [num]
	mov		rdx, [num]
	sub		rdx, 1
	and		rax, rdx

	cmp		rax, 0 ; n and (n - 1) == 0
	je		eqtrue
	jmp		eqfalse

eqtrue:
	mov		rsi, itstrue
	mov		rdx, itstrue.len
	jmp		out

eqfalse:
	mov		rsi, itsfalse
	mov		rdx, itsfalse.len
	jmp		out

out:
	mov		rax, 0x2000004 ; write
	mov		rdi, 1 ; stdout
	syscall

	mov		rax, 0x2000001 ; exit
	mov		rdi, 0
	syscall

