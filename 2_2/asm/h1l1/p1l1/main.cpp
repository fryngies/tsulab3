#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cstring>

int extract_grad(int num, int grad=1000, int step=10) {
    int rem = -1;

    asm(R"(
        divl %[grad]
        clr %[rem]
        divl %[step]
        )"
    : [rem]"=d" (rem) : "a" (num), "d" (0), [step]"rm" (step), [grad]"rm" (grad));
    
    if (rem == -1) {
        throw "ERR_REM_FAILED";
    }

    return rem;
}

int main() {
    srand((unsigned int)time(NULL));
    
    for(int i = 0; i < 8; ++i) {
        uint16_t num = rand() % 64535 + 1000;
        
        printf("extracting fourth num from %d... ", num);
        
        try {
            printf("found %d", extract_grad(num));
        } catch (char const* err) {
            printf("%s %s", "something went wrong: ", err);
        }
        printf("\n");
    }
    
    return 0;
}
