#include <iostream>
#include "cfstream.hpp"

void cfstream::copy(std::string to) {
	std::ios_base::streampos pos = tellg();
	std::ofstream dst(to.c_str());

	seekg(0);
	dst << rdbuf();
	dst.close();
	seekg(pos);
}

cfstream& cfstream::clearf() {
	close();
	open(name, std::fstream::trunc | om);

	return *this;
}

cfstream& cfstream::open() {
	cfstream::open(name, om);
	
	return *this;
}

cfstream& cfstream::open(std::string nname, openmode nom) {
	close();

	if (name != nname)
    	name = nname;
	if (om != nom)
    	om = nom;
	std::fstream::open(name.c_str(), om);
	if (!*this)
		throw std::runtime_error("Could not open file");

	return *this;
}

void cfstream::reset() {
	std::fstream::clear();
	seekg(0, std::ios::beg);
}

std::string cfstream::getname() {
	return name;
}

cfstream::openmode cfstream::getmode() {
	return om;
}

cfstream& cfstream::changemode(openmode nom) {
	if (nom != om) {
		om = nom;
		updatemode();
	}

	return *this;
}

cfstream& cfstream::appendmode(openmode aom) {
	return changemode(om | aom);
}

void cfstream::remove() {
	close();
	std::remove(name.c_str());
}

bool cfstream::isempty() {
	return peek() == std::fstream::traits_type::eof();
}
