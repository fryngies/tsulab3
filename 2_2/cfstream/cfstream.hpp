#include <fstream>

class cfstream : public std::fstream {
	typedef std::ios_base::openmode openmode;
	
	std::string name;
	openmode om;

	// TODO: Validate mode
	cfstream& updatemode() {
		close();
		cfstream::open(name, om);
		
		return *this;
	}
	
public:
	cfstream(std::string name, openmode om) : std::fstream(name.c_str(), om), name(name), om(om) {
		if (!*this)
			throw std::runtime_error("Could not open file");
	}
	
	void copy(std::string);
	cfstream& clearf();
//	cfstream& truncate();
	cfstream& open();
	cfstream& open(std::string, openmode);
	void reset();
	cfstream& changemode(openmode);
	cfstream& appendmode(openmode);
	void remove();
	
	bool isempty();
	
	std::string getname();
	openmode getmode();
	
	friend std::fstream;
};

