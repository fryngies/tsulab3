//////////////////////////////////////////////////////////////////////////////
//
//  File sort (polyphase merge)
//  (c) Johna Smith, 1996
//
//  Method description:
//   This program reads data from file 1.dat and writes sorted data to 1.srt
//   Format of 1.dat: i1 i2 i3 i4 i5 i6 ..., where i(k) is integer
//   It also creates four temporary files: 1.tmp, 2.tmp, 3.tmp
//   We use N files to sort given sequence of data.
//
//   Series - is a sequence of numbers with the following property: a(i)<=a(i+1)
//   1) Divide copy of the given file C into N-1 files, writing first
//      series from C to F1, second one to F2, third - to F3, and so on
//   2) Combine series from files F1,F2,F3,...,F(n-1) to Fn,
//      sorting distributed series
//   3) When we reach the end of one of F1..F(n-1) files we turn sequences
//      and will combine series to this new empty file. For example if we reach
//      end of F3 we will gather data from F1,F2,F4,..Fn to F3
//   4) Repeat first three steps until there is more than one series
//
//////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N   6  // number of temporary files (must be greater than 2)
struct ExtFile
{
  FILE *F;
  int first; // current element of this file
  char eor; // indicates end of series
};
  FILE *output;
  ExtFile input;
  ExtFile F[N];  // array of files
  int level,i,j,k,mx,tx,xmin,x,t[N],ta[N],a[N],d[N],z,tn,dn;
  char name[13];
// this functions sets new value of variable j
// on the next step of data distribution we'll write to sequence #j
void select(void)
{
  if (d[j]<d[j+1]) j++;  // d[j] shows number of series in sequence #j
  else
  {
    if (d[j]==0)
    {
      level++;
      z=a[0];
      for (int i=0;i<N-1;i++)
      {
        d[i]=z+a[i+1]-a[i];
        a[i]=z+a[i+1];
      }
    }
    j=0;
  }
  d[j]--;
}
// this function copies data element from x to y, updating (.first) members
void copy(ExtFile *x, ExtFile *y)
{
  y->first=x->first;
  fwrite(&y->first,sizeof(int),1,y->F);
  fread(&x->first,sizeof(int),1,x->F);
  x->eor=(x->first<y->first || feof(x->F));
}
// this function copies series from input to f[j]
void copyrun(void)
{
  do
  {
    F[j].first=input.first;
    fwrite(&F[j].first,sizeof(int),1,F[j].F);
    fscanf(input.F,"%d",&input.first);
    input.eor=input.first<F[j].first;
  } while (!input.eor && !feof(input.F));
  if (feof(input.F))
  {
    if (input.first<F[j].first) select();
    F[j].first=input.first;
    fwrite(&F[j].first,sizeof(int),1,F[j].F);
    d[j]++;
  }
}
int main(void)
{
  // opening required files (file 1.dat must exist)
  input.F=fopen("1.dat","rb");
  fscanf(input.F,"%d ",&input.first);
  F[t[N-1]].eor=0;
  output=fopen("1.srt","wb");
  for (int i=0;i<N-1;i++)
  {
    gcvt(i,8,name);
    strcat(name,".tmp");
    F[i].F=fopen(name,"w+b");
    fread(&F[i].first,sizeof(int),1,F[i].F);
    F[i].eor=0;
    a[i]=1;
    d[i]=1;
  }
  // initializing variables
  level=1;
  j=0;
  a[N-1]=0;
  d[N-1]=0;
  // distributing data from input to first N-1 sequences
  do
  {
    select();
    copyrun();
  } while (!feof(input.F) && j!=N-2);
  while (!feof(input.F))
  {
    select();
    if (F[j].first<=input.first)
    {
      copyrun();
      if (feof(input.F)) d[j]++; else copyrun();
    } else copyrun();
  }
  // preparing first N-1 series for reading
  for (i=0;i<N-1;i++)
  {
    t[i]=i;
    rewind(F[i].F);
    fread(&F[i].first,sizeof(int),1,F[i].F);
    F[i].eor=0;
  }
  t[N-1]=N-1;
  // main loop
  do
  {
    // gathering data from t[0]..t[N-2] into t[N-1]
    z=a[N-2];
    d[N-1]=0;
    // preparing file to write
    fclose(F[t[N-1]].F);
    gcvt(t[N-1],8,name);
    strcat(name,".tmp");
    F[t[N-1]].F=fopen(name,"w+b");
    F[t[N-1]].eor=0;
    do
    {
      // gathering one series
      k=-1;
      for (i=0;i<N-1;i++)
      {
        if (d[i]>0) d[i]--;
        else
        {
          k++;
          ta[k]=t[i];
        }
      }
      if (k==-1) d[N-1]++;
      else
      {
        // gathering one real series from t[0]..t[k] into t[N-1]
        do
        {
          i=0;
          mx=0;
          xmin=F[ta[i]].first;
          // selecting series with minimal element to place it first
          while (i<k)
          {
            i++;
            x=F[ta[i]].first;
            if (x<xmin)
            {
              xmin=x;
              mx=i;
            }
          }
          // copying smallest element to output series
          copy(&F[ta[mx]],&F[t[N-1]]);
          // if we reach the end of the current input sequence
          // or end of series in it then close this source
          if (F[ta[mx]].eor)
          {
            // closing this source
            ta[mx]=ta[k];
            k--;
          }
        } while (k!=-1);
      }  
      z--;
    } while (z!=0);
    // swapping sequences
    rewind(F[t[N-1]].F);
    fread(&F[t[N-1]].first,sizeof(int),1,F[t[N-1]].F);
    F[t[N-1]].eor=0;
    tn=t[N-1];
    dn=d[N-1];
    z=a[N-2];
    for(i=N-1;i>0;i--)
    {
      t[i]=t[i-1];
      d[i]=d[i-1];
      a[i]=a[i-1]-z;
    }
    t[0]=tn;
    d[0]=dn;
    a[0]=z;
    // preparing file F[t[N-1]] to write
    fclose(F[t[N-1]].F);
    gcvt(t[N-1],8,name);
    strcat(name,".tmp");
    F[t[N-1]].F=fopen(name,"w+b");
    F[t[N-1]].eor=0;
    level--;
  } while (level!=0); // while there are more than one series
  // writing sorted sequence from F[t[0]]
  rewind(F[t[0]].F);
  fread(&x,sizeof(int),1,F[t[0]].F);
  while (!feof(F[t[0]].F))
  {
    fprintf(output,"%d ",x);
    fread(&x,sizeof(int),1,F[t[0]].F);
  }
  // closing all opened files
  for (i=0;i<N;i++) fclose(F[i].F);
  fclose(input.F);
  fclose(output);
	return 0;
}
