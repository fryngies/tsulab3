#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include "../Boolv/boolv.h"
#include "../SetChar/setchar.hpp"
#include "../SetInt/setint.hpp"

using namespace std;

int main() {
	SetInt a(100);
	SetChar s;

	unsigned int k;
	char c;

	Boolv *p, v("110011100001");

	srand(time(0));

	for (int i = 0; i < 10; i++) {
		switch (rand() % 3) {
			case 0: {
				k = rand() % 100;
				a += k;
				p = &a;
				break;
			}
			case 1: {
				c = 'a' + rand() % 26;
				s += c;
				p = &s;
				break;
			}
			case 2:
				p = &v;
		}

		p->Out();
	}

	cout << a << s << endl;

	return 0;
}

