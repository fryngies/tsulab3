#include <iostream>
#include "setchar.hpp"

SetChar& SetChar::operator -=(SetChar& t) {
	int i = 0;
	while(!(CheckIndex(i) || t.CheckIndex(i))) {
		if(t[i])
			Set0(i);
		++i;
	}
	return *this;
}

std::ostream& operator <<(std::ostream& r, SetChar& s) {
	int i = 0;
	while(!s.CheckIndex(i)) {
		if(s[i])
			r << (char)i << " ";
		++i;
	}

	return r;
}

