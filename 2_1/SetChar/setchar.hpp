#ifndef SETCHAR_HPP
#define SETCHAR_HPP

#include <iostream>
#include "../Boolv/boolv.h"

class SetChar : public Boolv {
public:
	SetChar(int n=256) : Boolv(n) {}
	SetChar(const char* s) : Boolv(s) {}
	SetChar& operator +=(char c) {
		Set1((int)c);
		return *this;
	}

	SetChar& operator -=(SetChar& c); // Разность множеств

	int size() {
		return Weight();
	}
	bool empty() {
		return !Weight();
	}
	void Out() {
		std::cout << "SetChar: " << *this << std::endl;
	}

	friend std::ostream& operator << (std::ostream& r, SetChar& s);
};

#endif // SETCHAR_HPP

