#ifndef HANOI_H
#define HANOI_H

#include "../Stack/stack.h"

/**
 * @param *s - 3 стека с дисками
 * @param n - кол-во дисков, которое нужно переложить
 * @param from - откуда
 * @param to - куда
 * @param help - вспомогательный диск
 */
void hanoi_towers(Stack<int>* s, int n, int from, int to, int help);

#endif // HANOI_H

