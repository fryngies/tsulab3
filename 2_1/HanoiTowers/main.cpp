#include "../Stack/stack.h"
#include "hanoi.hpp"

int main()
{
	const int n = 4;
	Stack<int> a[3];

	for(int i = 0; i < n; ++i)
		a[0].push(n-i);

	hanoi_towers(a, n, 0, 2, 1);

	return 0;
}

