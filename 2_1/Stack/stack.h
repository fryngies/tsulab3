#ifndef STACK_H
#define STACK_H

#include <iostream>

template <class T> class Stack {
	int size_max;
	int k;

	T* s;

public:
	Stack(int n = 4);
	Stack(Stack<T>&);
	~Stack();

	Stack<T> operator =(Stack<T>&);

	void push(T);
	bool empty();
	T pop();
	T top();
	void out();
};

template <class T> Stack<T>::Stack(int n)
{
	s = new T[size_max=n];
	k = 0;
}

template <class T> Stack<T>::Stack(Stack<T>& t)
{
	s = new T[size_max = t.size_max];
	k = t.k;
	for(int i = 0; i < k; ++i)
		s[i] = t.s[i];
}

template <class T> inline Stack<T>::~Stack<T>()
{
	if(s)
		delete[] s;
	s = 0;
}

template <class T> Stack<T> Stack<T>::operator=(Stack<T>& t)
{
	if(&t == this)
		return *this;

	if(s)
		delete[] s;

	s = new T[size_max = t.size_max];
	k = t.k;
	for(int i = 0; i < k; ++i)
		s[i] = t.s[i];

	return *this;
}

template <class T> void Stack<T>::push(T el)
{
	if(k == size_max)
		throw "Stack overflow";
	s[k++] = el;
}

template <class T> bool Stack<T>::empty()
{
	return k ? 0 : 1;
}

template <class T> T Stack<T>::pop()
{
	if(empty())
		throw "Stack is empty";
	return s[--k];
}

template <class T> T Stack<T>::top()
{
	if(empty())
		throw "Stack is empty";
	return s[k - 1];
}

template <class T> void Stack<T>::out()
{
	for(int i = 0; i < k; ++i)
		std::cout << s[i] << " ";
	std::cout << std::endl;
}

#endif

