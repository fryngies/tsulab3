#include <cstdlib>
#include <iostream>
#include <ctime>
#include <algorithm>
#include <sstream>

#include "array.h"

using namespace std;

Array::Array(int mm, int t, int d)
{
    m = mm;
    a = new int[m];

    srand(time(0));

    switch (t) {
    case 1:
        for(int i = 0; i < m; ++i)
            a[i] = rand() % d;
        break;
    case 2:
        a[0] = rand() % 100;
        for(int i = 1; i < m; ++i)
            a[i] = a[i-1] + rand() % 10;
        break;
    case 3:
        a[m-1] = rand() % 100;
        for(int i = m-1; i > 0; --i)
            a[i-1] = a[i] + rand() % 10;
        break;
    default:
        break;
    }
}

Array::Array(int *b, int mm)
{
    m = mm;
    a = new int[m];

    for(int i = 0; i < m; ++i)
        a[i] = b[i];
}

Array::Array(Array &b)
{
    m = b.m;
    a = new int[m];

    for(int i = 0; i < m; ++i)
        a[i] = b[i];
}

Array::~Array()
{
    if (a)
        delete []a;
    a = 0;
}

int& Array::operator [](int i)
{
    return a[i];
}

Array& Array::operator +(Array &b)
{
    Array *d = new Array(m + b.m);
    Array &c = *d;

    int i, j, k;

    for(i = 0, j = 0, k = 0; i < m && j < b.m; ++k)
        if(a[i] <= b[j])
            c[k] = a[i++];
        else
            c[k] = b[j++];

    while(i < m)
        c[k++] = a[i++];
    while(j < b.m)
        c[k++] = b[j++];

    return c;
}

Array& Array::operator -(int val)
{
    for(int i = 0; i < m; )
        if(a[i] == val)
        {
            for(int j = i; j < m; ++j)
                a[j] = a[j+1];
            --m;
        } else {
            ++i;
        }

    int *b = new int[m];
    for(int i = 0; i < m; ++i)
        b[i] = a[i];
    delete []a;
    a = b;

    return *this;
}

Array& Array::operator =(Array &b)
{
    if(&b == this)
        return *this;

    if(a)
        delete []a;
    m = b.m;
    a = new int[b.m];

    for(int i = 0; i < b.m; ++i)
        a[i] = b[i];

    return *this;
}

void Array::operator ()(int k = 0)
{
    for(int i = 0; i < m; ++i)
        a[i] = k;
}

bool Array::operator ==(Array b)
{
    Array d = *this;

    if(d.m != b.m)
        return 0;

    for(int i = (d.m-1); i > 0; --i)
    {
        int tmp = d[i];

        d - tmp;
        b - tmp;

        if(d.m != b.m)
            return 0;
    }
    return 1;
}

inline void Array::swap(int i, int j)
{
	int buff = a[i];
	a[i] = a[j];
	a[j] = buff;
}

istream& operator >>(istream &r, Array &a)
{
    if(a.a)
        delete []a.a;

    cout << "length:\t";
    cin >> a.m;
    for (int i = 0; i < a.m; ++i)
    {
        cout << i << ":\t";
        r >> a[i];
    }

    return r;
}

ostream& operator <<(ostream &r, Array &a)
{
    for (int i = 0; i < a.m; ++i)
        r << a[i] << " ";

    return r;
}

void Array::BarrierSort()
{
    int *b = new int[m+1];
    for(int i = 0; i < m; ++i)
        b[i+1] = a[i];

    for(int i = 2, j; i <= m; ++i)
    {
        b[0] = b[i];
        j = i-1;

        while(b[j] > b[0])
        {
            b[j+1] = b[j];
            --j;
        }
        b[j+1] = b[0];
    }

    for(int i = 0; i < m; ++i)
        a[i] = b[i+1];
    delete []b;
}

void Array::ShakerSort()
{
	for (int left = 0, right = m-1; left < right;)
	{
		int buff;
		for (int i = left; i < right; ++i)
			if (a[i+1] < a[i])
				swap(i, buff=(i+1));

		right = buff;

		for (int i = right; i > left; --i)
			if (a[i-1] > a[i])
				swap(buff=(i-1), i);

		left = buff;
	}
}

void Array::QuickSort(int left, int right)
{
	int i = left, j = right;
	int pivot = a[(left + right) / 2];

	while (i <= j) {
		while (a[i] < pivot)
			++i;
		while (a[j] > pivot)
			--j;
		if (i <= j)
			swap(i++, j--);
	}

	if (left < j)
		QuickSort(left, j);
	if (i < right)
		QuickSort(i, right);
}

void Array::ShellSort()
{
	for (int k = m/2; k > 0; k /= 2)
		for (int i = k; i < m; ++i)
		{
			int buf = a[i];
			int j;
			for (j = i; j >= k && buf < a[j-k]; j -= k)
				a[j] = a[j-k];

			a[j] = buf;
		}
}

void Array::RadixSort(int left, int right, int radix) {
	if (left >= right || radix < 0)
		return;

	int mask = 1 << radix;
	int i = left;
	int j = right;

	// partition
	while (i <= j) {
		while (i <= j && !(a[i] & mask))
			++i;
		while (i <= j && (a[j] & mask))
			--j;

		if (i < j)
			swap(i++, j--);
	};

	// recursion
	if (left < j)
		RadixSort(left, j, radix-1);
	if (i < right)
		RadixSort(i, right, radix-1);
}

void Array::HeapInsert(int i, int n)
{
	while (2 * i + 1 < n)
	{
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		int j = right < n && a[right] > a[left] ? right : left;

		if(a[i] >= a[j])
			break;
		swap(i, j);
		i = j;
	}
}

void Array::BuildHeap()
{
	for (int i = (m-1)/2; i >= 0; --i)
		HeapInsert(i, m);
}

void Array::HeapSort()
{
	BuildHeap();
	for (int i = m-1; i > 0; --i)
	{
		swap(i, 0);
		HeapInsert(0, i);
	}
}
bool Array::Test()
{
    for(int i = 1; i < m; ++i)
        if(a[i-1] > a[i])
            return 0;
    return 1;
}

int Array::Max()
{
    int max = a[0];
    for(int i = 1; i < m; ++i)
        if(max < a[i])
            max = a[i];

    return max;
}
