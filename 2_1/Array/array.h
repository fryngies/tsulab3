#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <sstream>

class Array
{
    int *a; // массив
    int m; // размер

public:
    /* 1 - произвольный массив
     * 2 - упорядоченный по возрастанию
     * 3 - упорядоченный по убыванию */
    Array(int mm = 1, int t = 1, int d = 10);
    Array(int *, int);
    Array(Array&);
    ~Array();

    int &operator [](int); // размер массива
    Array& operator +(Array&); // сливает два упорядоченных массива в один упорядоченный
    Array& operator -(int); // удаляет из массива все значения
    Array& operator =(Array&); // присваивание
    void operator ()(int); // присвоить всем элементам k
    bool operator ==(Array); // сравнение

    friend std::istream& operator >>(std::istream&, Array&);
    friend std::ostream& operator <<(std::ostream&, Array&);

	void swap(int, int);

    // https://2654b451-a-62cb3a1a-s-sites.googlegroups.com/site/arraylazarus/sheme/sheme-04/4-04-1.png?attachauth=ANoY7cqadRHN1CcVg3nwFuL1VDUJkpjibsZ8VUsgh_Yq_o0y_4sImbXp5v9fcrm4f5CHRJAyfGEGZEx5cEUijRkyLnQcLGsolDtX0eFfASMdD1wz-47U8vETeTL0xzLKQ2SL_cXIsytpsMC4bHqiKzykTHcE-KTL0dJ-6dOI1gD42rUfsnUAtNADsmBAweEEcC4hS9kXKjymJfiqa0WaGAHjkWLoOW79orrj8FR8D98r0V7dd_FBSEw%3D&attredirects=0
    void BarrierSort(); // сортировка с барьером
	void ShakerSort();
	void QuickSort(int left, int right);
	void ShellSort();
	void RadixSort(int, int, int);
	void HeapInsert(int, int);
	void BuildHeap();
	void HeapSort();
    bool Test(); // упорядочен ли по неубыванию?
    int Max();
};

#endif // ARRAY_HPP
