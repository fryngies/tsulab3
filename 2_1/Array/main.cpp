#include <iostream>
#include <cmath>

#include "array.h"

using namespace std;

int main()
{
	const int d = 100;
    Array   arr_random(10, 1, d),
			arr_random1(10, 1, d),
			arr_random2(500, 1, d),
			arr_random3(500, 1, d),
			arr_random4(500, 1, d),
			arr_random5(500, 1, d),
			arr_random6(500, 1, d),
            arr_random_sorted(arr_random),
            arr_sorted_a(10, 2, d),
            arr_sorted_b(10, 2, d),
            arr_sorted_r(10, 3, d);

	Array arr_random2o(arr_random2);
	cout << "arr_random2 sorted: " << arr_random2.Test() << endl;
	arr_random2.ShakerSort();
	cout << "Shaker sorted arr_random2: " << arr_random2.Test() << endl;
	cout << "arr_random2 == arr_random2o: " << (arr_random2 == arr_random2o) << endl;

	cout << endl;

	Array arr_random3o(arr_random3);
	cout << "arr_random3 sorted: " << arr_random3.Test() << endl;
	arr_random3.QuickSort(0, 499);
	cout << "Quick sort arr_random3: " << arr_random3.Test() << endl;
	cout << "arr_random3 == arr_random3o: " << (arr_random3 == arr_random3o) << endl;

	cout << endl;

	Array arr_random4o(arr_random4);
	cout << "arr_random4 sorted: " << arr_random4.Test() << endl;
	arr_random4.ShellSort();
	cout << "Shell sort arr_random4: " << arr_random4.Test() << endl;
	cout << "arr_random4 == arr_random4o: " << (arr_random4 == arr_random4o) << endl;

	cout << endl;

	Array arr_random5o(arr_random5);
	cout << "arr_random5 sorted: " << arr_random5.Test() << endl;
	arr_random5.HeapSort();
	cout << "Heap sort arr_random5: " << arr_random5.Test() << endl;
	cout << "arr_random5 == arr_random5o: " << (arr_random5 == arr_random5o) << endl;

	cout << endl;

	Array arr_random6o(arr_random6);
	cout << "arr_random6 sorted: " << arr_random6.Test() << endl;
	arr_random6.RadixSort(0, 499, (int)log2(d)+1);
	cout << "Radix sort arr_random6: " << arr_random6.Test() << endl;
	cout << "arr_random6 == arr_random6o: " << (arr_random6 == arr_random6o) << endl;

    return 0;
}

