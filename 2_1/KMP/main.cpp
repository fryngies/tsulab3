#include <iostream>

using namespace std;

int KMP(const char *str_p, const char *str_s)
{
    int N = strlen(str_s),
        M = strlen(str_p);

    int *d = new int[M];

    d[0] = 0;
    for(int i = 1, j = 0; i < M; ++i)
    {
        while(j > 0 && str_p[j] != str_p[i])
            j = d[j-1];
        if(str_p[j] == str_p[i])
            j++;

        d[i] = j;
    }

    for(int i = 0, j = 0; i < N; ++i)
    {
        while(j > 0 && str_p[j] != str_s[i])
            j = d[j-1];
        if(str_p[j] == str_s[i])
            j++;

        if (j == M)
        {
            delete []d;

            return i-j+1;
        }
    }

    delete []d;

    return -1;
}

int main()
{
    const char **ts = new const char*[10];
    for(int i = 0; i < 10; ++i)
        ts[i] = new char[35];

    ts[0] = "abssfgb dfgbsdfg bfsdg";
    ts[1] = "dfg";

    ts[2] = "NONE";

    for(int i = 0; i < 10 && strcmp(ts[i], "NONE"); i += 2)
        cout << KMP(ts[i], ts[i+1]) << endl;

    return 0;
}
