#include <iostream>
#include <cmath>

using namespace std;

class complex
{
    float Re, Im;

public:
    complex(float re=0.0, float im=0.0)
    {
        Re = re;
        Im = im;
    }

    // Output
    void display()
    {
        cout    << Re
                << " + "
                << Im << "i"
                << endl;
    }

    // G/s
    float &Real()
    {
        return Re;
    }
    float &Image()
    {
        return Im;
    }
    float Mod()
    {
        return sqrt(Re * Re + Im * Im);
    }

    void Add(complex &b)
    {
        Re = Re + b.Re;
        Im = Im + b.Im;
    }
    void Sub(complex &b)
    {
        Re = Re - b.Re;
        Im = Im - b.Im;
    }

    bool Equal(complex &b)
    {
        return Re == b.Re && Im == b.Im;
    }
};

int main()
{
    complex a(5.4, 1.05);
    complex b(1.55);
    complex c;
    complex d = complex(7.0, 3.0);

    a.display();
    b.display();
    c.display();
    d.display();

    cout << endl;
    
    a.Add(b);
    cout << "a.Add(b): ";
    a.display();
    
    b.Sub(d);
    cout << "b.Sub(d): ";
    b.display();

    cout << b.Mod();
    b.Real() = 4;

    cout << d.Equal(c) << endl;
    
    return 0;
}
