#include "../Boolv/boolv.h"
#include "../SetChar/setchar.hpp"
#include "../SetInt/setint.hpp"

using namespace std;

Boolv* in() {
	Boolv* b;
	SetChar* sch;
	SetInt* a;
	char s[256];
	int n;
	Boolv* p;

	cout << "Bool vector: ";
    cin >> s;

    cout << "Type (0 - vector, 1 - char, 2 - num): ";
    cin >> n;

    switch (n % 3) {
	case 0: {
		b = new Boolv(s);
		p = b;
		break;
	}
	case 1: {
		sch = new SetChar(s);
		p = sch;
		break;
	}
	case 2: {
		a = new SetInt(s);
		p = a;
	}
	}

	return p;
}

int main() {
	unsigned int st = 5;
	Boolv* p;

	while(st--) {
		p = in();
		p->Out();
		delete p;
	}

	return 0;
}

