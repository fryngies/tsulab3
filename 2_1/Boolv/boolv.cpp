#include <iostream>
#include <cstring>
#include <cmath>
#include "boolv.h"

using namespace std;

// Boolv
Boolv::Boolv(int nn)
{
    nbit = nn;
    RecalcM();
    b = new byte[m];

    for(int i = 0; i < m; ++i)
        b[i] = 0;
}

Boolv::Boolv(Boolv &v)
{
    nbit = v.nbit;
    m = v.m;
    b = new byte[m];

    for(int i = 0; i < m; ++i)
        b[i] = v.b[i];
}

Boolv::Boolv(const char *s)
{
    nbit  = strlen(s);
    RecalcM();
    b = new byte[m];

    for(int i = 0; i < m; ++i)
        b[i] = 0;
    for(int i = 0; i < nbit; ++i)
        if(s[i] == '1')
            Set1(i);
}

Boolv::Boolv(const char *s, int nn)
{
    int s_len = strlen(s);

    nbit = nn;
    m = (nbit-1) / 8 + 1;
    b = new byte[m];

    for(int i = 0; i < m; ++i)
        b[i] = 0;
    for(int i = 0; i < s_len && i < nbit; ++i)
        if(s[i] == '1')
            Set1(i);
}

Boolv::~Boolv()
{
    if(b)
    {
        delete b;
        b = 0;
    }
}

inline void Boolv::RecalcM()
{
    m = (nbit-1)/8 + 1;
}

inline bool Boolv::CheckIndex(int k)
{
    return k < 0 || k >= nbit;
}

int Boolv::Weight()
{
    int k = 0;
    for(int i = 0; i < nbit; ++i)
        k += (*this)[i];

    return k;
}

void Boolv::Out()
{
	cout << "Boolv: " << *this << endl;
}

void Boolv::Set0(int k)
{
    if(CheckIndex(k))
        throw "Wrong index";

    int i = k / 8;

    b[i] = b[i] & ~(1 << (k % 8));
}

void Boolv::Set1(int k)
{
    if(CheckIndex(k))
        throw "Wrong index";

    int i = k / 8;

    b[i] = b[i] | (1 << (k % 8));
}

int Boolv::operator [](int k)
{
    if(CheckIndex(k))
        throw "Wrong index";

    int i = k / 8;

    if(b[i] & (1 << (k % 8)))
        return 1;
    return 0;
}

Boolv& Boolv::operator |(Boolv &v)
{
    int n = nbit > v.nbit ? nbit : v.nbit;

    Boolv *d = new Boolv(n);
    Boolv &g = *d;
    if(nbit > v.nbit)
        g = *this;
    else
        g = v;

    n = m > v.m ? m : v.m;
    for(int i = 0; i < n; ++i)
        g.b[i] = b[i] | v.b[i];

    return g;
}

Boolv& Boolv::operator &(Boolv &v)
{
    int n = nbit > v.nbit ? nbit : v.nbit;

    Boolv *d = new Boolv(v);
    Boolv &g = *d;
    if(nbit > v.nbit)
        g = *this;
    else
        g = v;

    n = m > v.m ? m : v.m;
    for(int i = 0; i < n; ++i)
        g.b[i] = b[i] & v.b[i];

    return g;
}

Boolv& Boolv::operator ^(Boolv &v)
{
    int n = nbit > v.nbit ? nbit : v.nbit;

    Boolv *d = new Boolv(v);
    Boolv &g = *d;
    if(nbit > v.nbit)
        g = *this;
    else
        g = v;

    n = m > v.m ? m : v.m;
    for(int i = 0; i < n; ++i)
        g.b[i] = b[i] ^ v.b[i];

    return g;
}

Boolv& Boolv::operator ~()
{
    Boolv *d = new Boolv(*this);
    Boolv &g = *d;

    for(int i = 0; i < g.m; ++i)
        g.b[i] = ~g.b[i];

    if(g.nbit % 8)
    {
        byte mask = (1 << (g.nbit % 8)) - 1;
        g.b[m+1] = g.b[m-1] & mask;
    }

    return g;
}

Boolv Boolv::operator =(Boolv &v)
{
    if(this == &v)
        return *this;

    if(b)
        delete []b;

    m = v.m;
    nbit = v.nbit;
    b = new byte[m];

    for(int i = 0; i < m; ++i)
        b[i] = v.b[i];

    return *this;
}

bool Boolv::operator ==(Boolv &v)
{
    if(nbit == v.nbit)
		return 0;
	
	int i;
	for(i = 0; i < m && b[i] == v.b[i]; ++i);

	return i == (m - 1);
}

ostream& operator <<(ostream &r, Boolv &v)
{
    for(int i = 0; i < v.nbit; ++i)
        cout << v[i];

    return r;
}

Boolv& Boolv::operator <<(int k)
{
    Boolv *v = new Boolv(nbit);

    for(int i = 0; i < (nbit - k); ++i)
        if((*this)[i+k])
            v->Set1(i);
        else
            v->Set0(i);

    for(int i = nbit - k; i < nbit; ++i)
        v->Set0(i);

    return *v;
}

Boolv& Boolv::operator >>(int k)
{
    Boolv *v = new Boolv(nbit);

    for(int i = k; i < nbit; ++i)
        if((*this)[i-k])
            v->Set1(i);
        else
            v->Set0(i);

    for(int i = 0; i < k; ++i)
        v->Set0(i);

    return *v;
}


// BoolM
BoolM::BoolM(int k, int l)
{
    m = k;
    v = new Boolv[m];

    n = l;
    Boolv w(n);

    for(int i = 0; i < m; ++i)
        v[i] = w;
}

BoolM::BoolM(BoolM &w)
{
    m = w.m;
    n = w.n;

    v = new Boolv[m];

    for(int i = 0; i < m; ++i)
        v[i] = w[i];
}

BoolM BoolM::operator =(BoolM &w)
{
    if(this == &w)
        return *this;

    if(v)
        delete []v;

    m = w.m;
    n = w.n;

    v = new Boolv[m];

    for(int i = 0; i < m; ++i)
        v[i] = w[i];

    return *this;
}

ostream& operator <<(ostream &r, BoolM &v)
{
    for(int i = 0; i < v.n; ++i)
        cout << v[i] << endl;

    return r;
}
