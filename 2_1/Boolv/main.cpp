#include <iostream>
#include <cmath>
#include "boolv.h"

using namespace std;

int main()
{
    const char *s1 = new char[30];
    s1 = "001011";

    Boolv a(s1);
    Boolv b(4);
    Boolv c(a);
    Boolv d(a | c);
    Boolv e = d;
    Boolv f("0001", 10);
    Boolv g("1001", 3);
    Boolv h;

    cout << "a(s1): "       << a << endl;
    cout << "\tWeight: "    << a.Weight() << endl;
    cout << "\ta[1]: "      << a[1] << endl;
    cout << "\t| c: "       << d << endl;
    cout << "\t~: "         << (~a) << endl;

    cout << endl;

    cout << "f(\"0001\", 10): "           << f << endl;
    cout << "g(\"1001\", 3): "           << g << endl;

    cout << endl;

    cout << "b(4): "        << b << endl;
    cout << "\tWeight: "    << b.Weight() << endl;
    cout << "\t[1]: "       << b[1] << endl;
    cout << "\t| c: "       << (b | c) << endl;
    cout << "\t~: "         << ~b << endl;

    cout << endl;

    b.Set1(1);
    cout << "b Set1(1): "   << b << endl;
    cout << "\tWeight: "    << b.Weight() << endl;
    cout << "\t[1]: "       << b[1] << endl;
    cout << "\t| c: "       << (b | c) << endl;
    cout << "\t~: "         << ~b << endl;

    cout << endl;

    cout << "c(a): "        << c << endl;
    cout << "d(a | c): "    << d << endl;
    cout << "e(d): "        << e << endl;

    cout << endl;

    cout << "a == c: "      << (a == c) << endl;
    cout << "b == b: "      << (b == b) << endl;
    cout << "b == e: "      << (b == e) << endl;

    cout << endl;

	cout << "a << 2: "		<< (a << 2) << endl;
	cout << "a >> 2: "		<< (a >> 2) << endl;

	cout << endl;

    BoolM matrix(5, 5);

    cout << "matrix: "      << matrix << endl;
    matrix[0].Set1(0);
    cout << "\t[0][0]: "    << matrix[0][0] << endl;

    return 0;
}
