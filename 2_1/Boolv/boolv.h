#ifndef BOOLV_H
#define BOOLV_H

#include <climits>
#include <sstream>

typedef unsigned char byte;

const int BYTE_MAX = UCHAR_MAX;

class Boolv
{
    // вектор
    byte *b;

    // m - количество элементов массива
    int m;
    // nbit - количество бит
    int nbit;

public:
    Boolv(int nn = 1); // пустой б.в. размера nn
    Boolv(const char*); // б.в. по строке
    Boolv(const char*, int); // б.в. по строке с заданной длинной
    // (int > const char* -> заполнить остаток нулями,
    //  int < const char* -> обрезать)
    Boolv(Boolv&); // конструктор копирования
    ~Boolv(); // деструктор

    void Set1(int); // установить бит в 1
    void Set0(int); // установить бит в 0

    bool CheckIndex(int k); // проверка допустимости передаваемого индекса
    void RecalcM(); // обновить число элементов
    int Weight(); // вес
	virtual void Out();

    int operator [](int);
    Boolv operator =(Boolv&);
    bool operator ==(Boolv&);

    Boolv& operator |(Boolv&);
    Boolv& operator &(Boolv&);
    Boolv& operator ~();
    Boolv& operator ^(Boolv&);
    Boolv& operator <<(int);
    Boolv& operator >>(int);

//    void Boolv::Out(); // вывод для виртуальной функции

    friend std::ostream& operator <<(std::ostream&, Boolv&);
};

class BoolM
{
    Boolv *v; // массив векторов
    int m; // число строк
    int n; // число столбцов

public:
    BoolM(int k, int l);
    BoolM(BoolM&);

    ~BoolM()
    {
        if(v)
            delete []v;
        v = 0;
    }

	int Width() {
		return n;
	}

    Boolv& operator [](int l)
    {
        if (l < 0 || l >= m)
            throw "Wrong index";
        return v[l];
    }
    BoolM operator =(BoolM&);

    friend std::ostream& operator <<(std::ostream&, BoolM&);
};

#endif // BOOLV_H
