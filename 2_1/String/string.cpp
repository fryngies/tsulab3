#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include "string.h"

String::String(int l)
{
	line = new char[l];
	len = 0;

	line[0] = '\0';
}

String::String(String &s)
{
	len = s.len;
	line = new char[len + 1];
	strcpy(line, s.line);
}

String::String(const char *s)
{
	len = strlen(s);
	line = new char[len + 1];
	strcpy(line, s);
}

String::~String()
{
	if(line)
		delete []line;
}

int String::Length()
{
	return len;
}

inline bool String::CheckIndex(int k)
{
	return k < 0 || k > len;
}

char& String::operator [](int k)
{
	if(CheckIndex(k))
		throw "Wrong index";

	return line[k];
}

String& String::operator ~()
{
	String *bs = new String(*this);
	for(int i = 0; i < len / 2; ++i)
	{
		char buffer = bs->line[i];
		bs->line[i] = bs->line[bs->len - i - 1];
		bs->line[bs->len - i - 1] = buffer;
	}

	return *bs;
}

String& String::operator+(String &s2)
{
	int nl = len + s2.len + 1;
	String *ns = new String(nl);

	strcpy(ns->line, line);
	strcat(ns->line, s2.line);
	ns->len = nl;

	return *ns;
}

String& String::operator=(String &s2)
{
	if(&s2 == this)
		return *this;

	delete []line;

	len = s2.len;
    line = new char[len + 1];
	for(int i = 0; i < len; ++i)
		line[i] = s2.line[i];
	line[len] = '\0';

	return *this;
}

bool String::operator ==(String &s2)
{
    if(strcmp(line, s2.line))
		return 0;
	return 1;
}

String& String::operator+=(String &s2)
{
	char *nl = new char[len + s2.len - 1];

	strcat(nl, line);
	delete []line;
	strcat(nl, s2.line);

	line = nl;

	return *this;
}

String& String::operator ++()
{
	for(int i = 0; i < len; ++i)
		++line[i];

	return *this;
}

String::operator char *()
{
	return line;
}

String::operator int()
{
	int ret = 0;

	for(int i = 0; i < len; ++i)
		ret += pow(10, len - i - 1) * line[i] - '\0';

	return ret;
}

std::ostream& operator <<(std::ostream &r, String &s2)
{
	r << s2.line;

	return r;
}

std::istream& operator >>(std::istream &r, String &s2)
{
	char *buffer = new char[256];

	r >> buffer;

	delete []s2.line;

	s2.len = strlen(buffer);
	s2.line = new char[s2.len + 1];
	strcpy(s2.line, buffer);

	return r;
}

