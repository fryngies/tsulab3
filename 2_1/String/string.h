#ifndef STRING_H
#define STRING_H

#include <cstdlib>
#include <iostream>

class String {
	char *line;
	int len;

public:
	String(int l=80);
	String(const char*);
	String(String&);
	~String();

	int Length();
	bool CheckIndex(int);

	char& operator [](int);
    bool operator ==(String &s);
	String& operator ~();
	String& operator +(String&);
	String& operator =(String&);
	String& operator +=(String &s2);
	String& operator ++();
	operator char *(); // преобразование к обычной строке
	operator int();

	friend std::ostream &operator <<(std::ostream&, String&);
	friend std::istream &operator >>(std::istream&, String&);
};

#endif

