#include <iostream>
#include "string.h"

using namespace std;

int main()
{
	String a("whoa");
	String b(", ");
	String c("wow");
	String d(9);

	cout << "a: " << a << endl;
	cout << "\tcin a: ";
	cin >> a;
	cout << "a: " << a << endl;
	cout << "\t~a: " << ~a << endl;
	cout << "\ta == a: " << (a == a) << endl;
	d = a;
	cout << "\td += b + c: " << (d += b + c) << endl;
	cout << "\td == a: " << (d == a) << endl;
	cout << "\t++a: " << ++a << endl;
	cout << "\tint(a): " << int(a) << endl;

    return 0;
}

