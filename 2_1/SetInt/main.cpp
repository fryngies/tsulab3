#include <iostream>
#include "../Boolv/boolv.h"
#include "setint.hpp"

using namespace std;

int main()
{
	SetInt set1(100), set2(100);

	(set1 += 68) += 97;
	cout << "set1: " << set1 << endl;

	(set2 += 45) += 18;
	cout << "set2: " << set2 << endl;

	cout << "set1 & set2: " << *(new SetInt(set1 & set2)) << endl
		 << "set1 | set2: " << *(new SetInt(set1 | set2)) << endl;
	cout << "set1 -= set2: " << (set1 -= set2) << endl;
	cout << (set1 == set2 ? "set1 == set2" : "set1 != set2") << endl
		 << "set1: " << set1 << endl
		 << "set2: " << set2 << endl;

	return 0;
}

