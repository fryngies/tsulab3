#ifndef SETINT_HPP
#define SETINT_HPP

#include <iostream>
#include "../Boolv/boolv.h"

class SetInt : public Boolv
{
public:
	SetInt(int n=1) : Boolv(n) {}
	SetInt(const char* s) : Boolv(s) {}
	SetInt(Boolv& v) : Boolv(v) {}

	SetInt& operator +=(int c); // Добавление элемента
	SetInt& operator -=(SetInt& c); // Разность множеств

	int size() {return Weight();}
	bool empty() {return !Weight();}

	void Out() {
		std::cout << "SetInt: " << *this << std::endl;
	}

	friend std::ostream& operator << (std::ostream& r, SetInt& s);
};

#endif // SETINT_HPP

