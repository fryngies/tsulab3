#include <iostream>
#include <fstream>
#include <vector>
#include "graphl.hpp"

using namespace std;

Leader* TopList::operator +(int k) {
	Leader* r = head;

	while(r && r->key != k)
		r = r->next;

	if(r)
		return r;

	r = new Leader(k, head);
	head = r;

	return r;
}

void TopList::ShowKey() {
	Leader* p = head;

	while(p) {
		cout << p->key << ' ';
		p = p->next;
	}
}

TopList::~TopList() {
	Leader* p = head;
	Leader* q = p;
	Trailer* t;
	Trailer* r;

	while(p) {
		p = p->next;
		t = q->tr;

		delete q;

		q = p;
		r = t;

		while (t) {
			t = t->next;

			delete r;

			r = t;
		}
	}
}

TopList& TopList::TopSort() {
    Leader* p;
    Leader* q;
    Leader* f = new Leader;
    Leader* l;
    TopList* Ldnew = new TopList;
    Trailer* t;

    f->next = head;
    Ldnew->head = new Leader;
    Ldnew->head->next = NULL;
    l = Ldnew->head;

    while(f->next) {
        p = f;

        while(p->next)
            if(p->next->count)
                p = p->next;
            else {
                q = p->next;
                p->next = q->next;
                t = q->tr;

                while(t) {
                    t->ld->count--;
                    t = t->next;
                }

                l->next = q;
                q->next = NULL;
                l = l->next;
            }
    }

    l = Ldnew->head;
    Ldnew->head = Ldnew->head->next;
    head = NULL;

    delete f;
    delete l;

    return *Ldnew;
}

void TopList::ReadList(char const* fname) {
	ifstream f(fname);
	if(!f)
		throw "Failed to read file";

	int buff1;
	int buff2;

	Leader* pa;
	Leader* pb;

	f >> buff1 >> buff2;

	while(buff1 && buff2) {
		pa = (*this) + buff1;
		pb = (*this) + buff2;
		++(*pb);
		*pa += pb;
		f >> buff1 >> buff2;
	}

	f.close();
}

void TopList::RemoveEven() {
	Leader* p = head;

	while(p) {
		if(( p->key % 2 ) != 0) {
			p = p->next;
			continue;
		}
		std::cout << 1 << std::endl;

		Trailer* p_tr = p->tr;

		while(p_tr) {
			--(p_tr->ld->count);
			p_tr = p_tr->next;
		}

		// std::cout << 1 << std::endl;

		Leader* p_r = head;

		while(p->count) {
			Trailer* p_tr = p_r->tr;
			Trailer* p_tr_prev = p_tr;

			if(!p_tr) {
				p_r = p_r->next;
				continue;
			}

			if(p_tr->ld == p) {
				p_r->tr = p_tr->next;
				delete p_tr;

				--(p->count);
			}

			while((p_tr = p_tr->next)) {
				if(p_tr->ld != p) {
					p_tr_prev = p_tr;
					continue;
				}

				p_tr_prev->next = p_tr->next;
				delete p_tr;
				p_tr = p_tr_prev;

				--(p->count);
			}
			p_r = p_r->next;
		}

		// delete p;
		p = p->next;
	}
}

ostream& operator <<(ostream& r, Leader& p) {
	r << p.key << '\t' << p.count << '\t';
	Trailer* t = p.tr;

	while (t) {
		r << t->ld->key << '\t';
		t = t->next;
	}

	return r;
}

ostream& operator <<(ostream& r,TopList& t) {
	Leader* s = t.head;

	r << "key\tcount\ttrailer:";

	while (s) {
		r << endl;
		r << *s;
		s = s->next;
	}

	return r;
}

