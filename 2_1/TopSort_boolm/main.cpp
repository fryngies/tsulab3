#include "../Boolv/boolv.h"
#include "../SetInt/setint.hpp"
#include <fstream>
#include <iostream>
#include <stdio.h>

using namespace std;

BoolM* ReadFile(char const* fname) {
	ifstream f(fname);

	if(!f)
		throw "Failed to open file";

	int n;
	int buff1;
	int buff2;

	f >> n;
	f >> buff1 >> buff2;

	BoolM* b = new BoolM(n, n);

	while(buff1 && buff2) {
		(*b)[buff1 - 1].Set1(buff2 - 1);

		cout << buff1 << ' ' << buff2 << endl;

		f >> buff1 >> buff2;
	}

	return b;
}

int* TopSort(BoolM* b, int n) {
	int* a = new int[n];
	int j = 0;

    Boolv v0(n); // обработанные вершины
	Boolv v1(n); // дизъюнкция строк
	Boolv v2(n); // новые вершины
	Boolv null_v(n);

	for(int i = 0; i < n; ++i)
		v1 = v1 | (*b)[i];

    v2 = (~v1) & (~v0);

    while(v2.Weight()) {
		for(int i = 0; i < n; ++i)
			if(v2[i]) {
				(*b)[i] = null_v;
				v0.Set1(i);
				a[j++] = i + 1;
			}

		v1 = null_v;
		for(int i = 0; i < n; i++)
			v1 = v1 | (*b)[i];

		v2 = (~v1) & (~v0);
    }

	return a;
}

int main() {
	BoolM* b;
	int* a;

    try {
		b = ReadFile("graph.txt");
    } catch (char const* s) {
		cout << s << endl;
    }

	int n = b->Width();
	a = TopSort(b, n);

	for(int i = 0; i < n; i++)
		cout << a[i] << ' ';
	cout << endl;

	return 0;
}

