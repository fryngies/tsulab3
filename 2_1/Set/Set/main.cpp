#include <iostream>
#include "set.h"

int main() {
    Set<int> a;
	Set<int> b;
    
	std::cout << "a:" << std::endl;
	std::cin >> a;
	std::cout << "b:" << std::endl;
	std::cin >> b;

    a += 1;
	std::cout << "a += 1: " << a << std::endl;
	std::cout << "a & b: " << (a & b) << std::endl;
	a -= b;
    std::cout << "a -= b: " << a << std::endl;
    
    return 0;
}

