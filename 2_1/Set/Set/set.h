#ifndef SET_HPP
#define SET_HPP

#include <iostream>
#include "../../Boolv/boolv.h"

template <class X>
class Set;

template <class X>
std::istream& operator >>(std::istream& r, Set<X>& b);

template <class X>
std::ostream& operator <<(std::ostream& r, Set<X>& b);

template <class X>
class Set
{
    Boolv v;
    int m; // Размер булевого вектора - максимальный диапазон значений
public:
    Set(int n=256) : v(n), m(n) {}
    Set(Set<X>& a) : v(a.v), m(a.m) {}
    
    Set<X>& operator &(Set<X>&); // Пересечение множеств
    Set<X>& operator |(Set<X>&); // Объединение множеств
    Set<X>& operator +=(X c); // Добавление элемента
    Set<X>& operator -=(Set<X>& c); // Разность множеств
    bool operator ==(Set<X> t) {
        return v == t.v;
    }
    
    int size() {
        return v.Weight();
    }
    bool empty() {
        return !v.Weight();
    }
    
    friend std::istream& operator >> <X>(std::istream& r, Set<X>& s);
    friend std::ostream& operator << <X>(std::ostream& r, Set<X>& s);
};

template <class X> Set<X>& Set<X>::operator &(Set<X>& t) {
    Set<X>* r = new Set(m < t.m ? m : t.m);
	r->v = v & t.v;

    return *r;
}

template <class X> Set<X>& Set<X>::operator |(Set<X> &t) {
    Set<X>* r = new Set<X>(m > t.m ? m : t.m);
    r->v = v | t.v;

    return *r;
}

template <class X> Set<X>& Set<X>::operator +=(X c) {
    int k = (int)(sizeof(c) == sizeof(char) ? byte(c) : c);
    
    if(k < m)
        v.Set1(k);
    else {
        Set<X> d(k+1);
		d.v.Set1(k);
        *this = d | *this;
    }

    return *this;
}

template <class X> Set<X>& Set<X>::operator -=(Set<X>& t) {
    int k = m < t.m ? m : t.m;
    for (int i = 0; i < k; ++i)
        if(t.v[i])
            this->v.Set0(i);

    return *this;
}

template <class X> std::istream& operator >>(std::istream& r, Set<X>& s) {
    X buf;
	int l;
	std::cout << "Enter length: ";
	r >> l;

    while(l-- != 0) {
        r >> buf;
        s += buf;
    }

    return r;
}

template <class X> std::ostream& operator <<(std::ostream& r, Set<X>& s) {
    for (int i = 0; i < s.m; ++i)
        if(s.v[i])
            r << (X)i << " ";

    return r;
}

#endif // SET_HPP

