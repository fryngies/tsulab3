//
//  List.cpp
//  nov18
//
//  Created by Дмитрий on 19.11.16.
//  Copyright © 2016 TomEngine. All rights reserved.
//

#include "list.hpp"
#include "set.h"

inline int& Node::key() {
    return x;
}

List::List() {
    // Фиктивная голова и фиктивный хвост
    beg = new Node;
    end = new Node;
    beg->next = end;
    end->prev = beg;
    beg->prev = end->next = nullptr;
}

List::List(List& l) {
    Node* p;
    Node* q;
    Node* t;
    
    beg = new Node;
    beg->prev = NULL;
    
    p = l.beg->next;
    q = beg;
    
    while(p) {
        t = new Node;
        t->key() = p->key();
        t->prev = q;
        q->next = t;

        q = t;
        p = p->next;
    }
    q->next = NULL;
    
    end = q;
}

List::List(int* arr, int l) {
    // Фиктивная голова и фиктивный хвост
    beg = new Node;
    end = new Node;
    beg->next = end;
    end->prev = beg;
    beg->prev = end->next = nullptr;
    
    for(int i = 0; i < l; ++i)
        *this += arr[i];
}

List::~List() {
    while(beg != end) {
        Node* p = beg;
        beg = beg->next;
        delete p;
    }
}

Node* List::find(int k) {
    Node* p;
    p = beg->next;
    
    while(p != end) {
        if(p->key() == k)
            return p;
        p = p->next;
    }

    return nullptr;
}

bool List::isEmpty() {
    return (beg->next == end);
}

List& List::cleanMinMax() {
    List* nl = new List(*this);
    Node* p;
    bool flag = false;
    Node* min;
    Node* max;
    min = max = nl->beg->next;

    for(p = nl->beg->next; p != nl->end; p = p->next) {
        if(min->key() > p->key()) {
            min = p;
            flag = true;
        }
        if(max->key() < p->key()) {
            max = p;
            flag = false;
        }
    }

    Node* from = flag ? max : min;
    Node* to = flag ? min : max;
    p = from->next;
    while(p != to) {
        Node *q = p;
        p = p->next;
        delete q;
    }
    from->next = to;
    to->prev = from;
    
    return *nl;
}

List& List::operator =(List& l) {
    if(this == &l)
        return *this;

    this->~List();
    List* new_list = new List(l);
    beg = new_list->beg;
    end = new_list->end;

    return *this;
}

Node& List::operator [](int n) {
    Node *tn = beg->next;

    for(int i = 0; i < n; ++i)
        tn = tn == end ? beg->next : tn->next;

    return *tn;
}

List& List::operator +(int val) {
    List *nl = new List(*this);
    Node *nn = new Node(val);
    
    nn->next = nl->beg->next;
    nn->prev = nl->beg;
	nl->beg->next->prev = nn;
    nl->beg->next = nn;
    
    return *nl;
}

List& List::operator +(List l) {
    List *nl = new List(*this);
    
    nl->end->next = l.beg->next;
    nl->end = l.end;
    
    l.beg->next = nullptr;
    
    return *nl;
}

List& List::operator -(int val) {
    List *nl = new List(*this);
    
    for(Node* tn = nl->beg->next; tn; tn = tn->next) {
        if(tn->key() != val)
            continue;
        
        tn->prev->next = tn->next;
        tn->next->prev = tn->prev;
        
        delete tn;
    }
    
    return *nl;
}

List& List::operator -=(int val) {
    for(Node* p = beg->next; p; p = p->next) {
        if(p->key() != val)
            continue;

        p->prev->next = p->next;
        p->next->prev = p->prev;
        
        delete p;
    }
    
    return *this;
}

List& List::operator +=(int val) {
    Node* nn = new Node(val);

    nn->prev = end->prev;
    nn->next = end;
    nn->prev->next = nn;
    end->prev = nn;

    return *this;
}

bool List::operator ==(List& l) {
    Set<int> s1;
    Set<int> s2;
    
    for(Node* p1 = beg->next; p1 != end; p1 = p1->next)
        s1 += p1->key();
    
    for(Node* p2 = l.beg->next; p2 != l.end; p2 = p2->next)
        s2 += p2->key();
    
    return s1 == s2;
}

std::istream& operator >>(std::istream& r, Node& n) {
    return (r >> n.key());
}

std::ostream& operator <<(std::ostream& r, Node& n) {
    return (r << n.key());
}

std::istream& operator >>(std::istream& r, List& l) {
    l.~List();
    l = *new List;

    int k;

    std::cout << "Enter length: ";
    std::cin >> k;

    for(int i = 0, j; i < k; ++i) {
        std::cin >> j;
        l += j;
    }

    return r;
}

std::ostream& operator <<(std::ostream& r, List& l) {
    if(l.isEmpty())
        return r;

    for (Node* p = l.beg->next; p != l.end; p = p->next)
        r << *p << " ";
    return r;
}

