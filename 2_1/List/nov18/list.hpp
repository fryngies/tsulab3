//
//  List.hpp
//  nov18
//
//  Created by Дмитрий on 19.11.16.
//  Copyright © 2016 TomEngine. All rights reserved.
//

#ifndef list_hpp
#define list_hpp

#include <iostream>

class List;

class Node {
    int x;
    Node* prev = nullptr;
    Node* next = nullptr;
    
    friend class List;
    friend std::istream& operator >>(std::istream&, Node&);
    friend std::ostream& operator <<(std::ostream&, Node&);
    friend std::istream& operator >>(std::istream&, List&);
    friend std::ostream& operator <<(std::ostream&, List&);
    
public:
    Node() : x(0) {}
    Node(int k) : x(k) {}
    int& key();
};

class List {
    Node* beg;
    Node* end;
    
public:
    List();
    List(int*, int); // список по массиву
    List(List& l);
    ~List();
    
    Node* find(int k);
    bool isEmpty();
    List& cleanMinMax(); // найти максимальный и минимальный объекты и удалить все объекты между ними
    
    List& operator =(List&);
    Node& operator [](int);
    List& operator +(int);
    List& operator +(List);
    List& operator -(int);
    List& operator -=(int); // удаление из списка заданных ключей
    List& operator +=(int);
    bool operator ==(List&);
    
    friend std::istream& operator >>(std::istream&, List&);
    friend std::ostream& operator <<(std::ostream&, List&);
};


#endif /* list_hpp */
